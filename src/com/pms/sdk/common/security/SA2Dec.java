package com.pms.sdk.common.security;

import com.pms.sdk.common.compress.CZip;

public class SA2Dec {

	private static String _USER_KEY = "amailRND2009";

	public static final int KEY_LENGTH = 16;
	public static final int KEY_START_INDEX = 17;

	public static String decrypt (String str) {
		return decrypt(str, _USER_KEY);
	}

	public static String decrypt (String str, String userKey) {

		if (str == null || userKey == null || str.length() < KEY_LENGTH + KEY_START_INDEX)
			return null;

		try {

			byte[] byteDecBase64 = Base64.decode(str);
			byte[] decStr = null;
			if ( userKey == null )
				decStr = Seed.seedDecrypt(byteDecBase64);
			else
				decStr = Seed.seedDecrypt(byteDecBase64, userKey.getBytes());
			String strCzip = CZip.unzipStringFromBytes(decStr);
			return strCzip;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {

		}
	}
}
