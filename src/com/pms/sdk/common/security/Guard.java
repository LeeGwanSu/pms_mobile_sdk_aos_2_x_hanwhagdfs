package com.pms.sdk.common.security;

public class Guard
{
    private static String[] c1 = new String[]{"r", "v", "n", "s", "m", "d", "o", "u", "H", "_", "-"};
    private static String[] c2 = new String[]{"e", "g", "k", "n", "s", "y", "C", "E", "P", "_", "-"};
    private static int[] r = new int[]{320, 412, 180, 460, 380, 276, 380, 440, 380, 268, 380, 428, 380, 404, 380, 484};
    private static int[] l = new int[]{48, 57, 74, 88, 110, 136, 96, 105, 55, 121, 99};

    protected static String getCharacters(int loc)
    {
        try
        {
            return c2[getLocation(loc)];
        }
        catch (Exception e) { }

        return null;
    }

    private static int getLocation(int loc)
    {
        for (int i=0; i<l.length; i++)
        {
            if (l[i] == loc)
            {
                return i;
            }
        }

        return -1;
    }

    private static int getLocation2(int loc)
    {
        for (int i=0; i<r.length; i++)
        {
            if (r[i] == loc)
            {
                return i;
            }
        }

        return -1;
    }

    protected static int getLocation(String chars)
    {
        for (int i=0; i<c1.length; i++)
        {
            if (c1[i].equals(chars))
            {
                return l[i];
            }
        }

        return -1;
    }
}
