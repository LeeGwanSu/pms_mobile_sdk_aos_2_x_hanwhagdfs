package com.pms.sdk.common.security;

import java.util.ArrayList;
import java.util.List;

public class Decrypt extends Guard
{
    public static String run(String key)
    {
        try
        {
            char[] d = key.toCharArray();
            List<Integer> a = new ArrayList<>();
            for (int i=0; i<key.toCharArray().length; i++)
            {
                a.add(getLocation(String.valueOf(d[i])));
            }

            StringBuilder sb = new StringBuilder();
            for (int v : a)
            {
                sb.append(getCharacters(v));
            }

            return sb.toString();
        }
        catch (Exception e) { }

        return null;
    }
}
