package com.pms.sdk.api.request;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DataKeyUtil;
import com.pms.sdk.common.util.DateUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.push.FCMRequestToken;
import com.pms.sdk.push.mqtt.MQTTService;
import com.pms.sdk.push.mqtt.RestartReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;

public class DeviceCert extends BaseRequest {

	public DeviceCert(Context context) {
		super(context);
	}

	/**
	 * get param
	 * 
	 * @return
	 */
	public JSONObject getParam () {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("appKey", PMSUtil.getApplicationKey(mContext));
			jobj.put("uuid", PMSUtil.getUUID(mContext));
			jobj.put("pushToken", PMSUtil.getGCMToken(mContext));
			jobj.put("appVer", PhoneState.getAppVersion(mContext));
			jobj.put("custId", "");
			jobj.put("sdkVer", PMS_VERSION);
			jobj.put("os", "A");
			jobj.put("osVer", PhoneState.getOsVersion());
			jobj.put("device", PhoneState.getDeviceName());
			jobj.put("mktVer", FLAG_Y);
			jobj.put("sessCnt", "1");

			return jobj;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (final APICallback apiCallback) {

		//2019.09.05 손보광 업체 요청으로 인한 PREF 암호화 키 제거
		Prefs prefs = new Prefs(mContext);
		String prefEncKey = prefs.getString(IPMSConsts.PREF_ENC_KEY);
		if(!TextUtils.isEmpty(prefEncKey))
		{
			prefs.putString(PREF_ENC_KEY, "");
		}
		String prefDecKey = prefs.getString(IPMSConsts.PREF_DEC_KEY);
		if(!TextUtils.isEmpty(prefDecKey))
		{
			prefs.putString(PREF_DEC_KEY, "");
		}

		// pushToken이 set되기 전까지 sleep이 있어야 하기 때문에, 다른 api와 달리 AsyncTask로 만듬
		String custId = PMSUtil.getCustId(mContext);

		boolean isNExistsUUID = PhoneState.createDeviceToken(mContext);
		CLog.i("Device Cert request get UUID : " + (isNExistsUUID == true ? "Created" : "Exists"));

		// 토큰 존재 하지 않으면 NoToken으로 설정
		if (StringUtil.isEmpty(PMSUtil.getGCMToken(mContext)) || NO_TOKEN.equals(PMSUtil.getGCMToken(mContext))) {
			// check push token
			CLog.i("DeviceCert:no push token");
			DataKeyUtil.setDBKey(mContext, DB_GCM_TOKEN, NO_TOKEN);
			new FCMRequestToken(mContext, PMSUtil.getGCMProjectId(mContext), new FCMRequestToken.Callback() {
				@Override
				public void callback(boolean isSuccess, String message)
				{
					CLog.d("FCMRequestToken isSuccess? "+isSuccess+ " message:"+message);
					try
					{
						apiManager.call(API_DEVICE_CERT, getParam(), new APICallback() {
							@Override
							public void response (String code, JSONObject json) {
								if (CODE_SUCCESS.equals(code)) {
									PMSUtil.setDeviceCertStatus(mContext, DEVICECERT_COMPLETE);
									requiredResultProc(json);
								}

								apiCallback.response(code, json);
							}
						});
					}
					catch (Exception e)
					{
						e.printStackTrace();
					}
				}
			}).execute();
		}
		else
		{
			try
			{
				apiManager.call(API_DEVICE_CERT, getParam(), new APICallback() {
					@Override
					public void response (String code, JSONObject json) {
						if (CODE_SUCCESS.equals(code)) {
							PMSUtil.setDeviceCertStatus(mContext, DEVICECERT_COMPLETE);
							requiredResultProc(json);
						}

						apiCallback.response(code, json);
					}
				});
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

		CLog.i("DeviceCert:validate ok");
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	@SuppressLint("DefaultLocale")
	private boolean requiredResultProc (JSONObject json) {
		try {
			PMSUtil.setAppUserId(mContext, json.getString("appUserId"));
			PMSUtil.setEncKey(mContext, json.getString("encKey"));
			PMSUtil.setDecKey(mContext, json.getString("encKey"));
			if (json.has("license")) {
				PMSUtil.setLicenseFlag(mContext, json.getString("license"));
				if (LICENSE_EXPIRED.equals(PMSUtil.getLicenseFlag(mContext)) || LICENSE_PENDING.equals(PMSUtil.getLicenseFlag(mContext))) {
					mContext.stopService(new Intent(mContext, MQTTService.class));
				}
			}

			if (json.has("sdkHalt")) {
				PMSUtil.setSDKLockFlag(mContext, json.getString("sdkHalt"));
				if (FLAG_Y.equals(PMSUtil.getSDKLockFlag(mContext))) {
					mContext.stopService(new Intent(mContext, MQTTService.class));
				}
			}

			// set msg flag
			DataKeyUtil.setDBKey(mContext, DB_API_LOG_FLAG, json.getString("collectApiLogFlag"));
			DataKeyUtil.setDBKey(mContext, DB_PRIVATE_LOG_FLAG, json.getString("collectPrivateLogFlag"));
			String custId = PMSUtil.getCustId(mContext);
			if (!StringUtil.isEmpty(custId)) {
				DataKeyUtil.setDBKey(mContext, DB_LOGINED_CUST_ID, PMSUtil.getCustId(mContext));
			}

			// set config flag
			final String mflag = json.getString("msgFlag");
			final String nflag = json.getString("notiFlag");
			final String mkflag = json.getString("mktFlag");

			if (StringUtil.isEmpty(DataKeyUtil.getDBKey(mContext, DB_MSG_FLAG))) {
				DataKeyUtil.setDBKey(mContext, DB_MSG_FLAG, mflag);
			}
			if (StringUtil.isEmpty(DataKeyUtil.getDBKey(mContext, DB_NOTI_FLAG))) {
				DataKeyUtil.setDBKey(mContext, DB_NOTI_FLAG, nflag);
			}
			if (StringUtil.isEmpty(DataKeyUtil.getDBKey(mContext, DB_MKT_FLAG))) {
				DataKeyUtil.setDBKey(mContext, DB_MKT_FLAG, mkflag);
			}

			if ((DataKeyUtil.getDBKey(mContext, DB_MSG_FLAG).equals(mflag) == false)
					|| (DataKeyUtil.getDBKey(mContext, DB_NOTI_FLAG).equals(nflag) == false)
					|| (DataKeyUtil.getDBKey(mContext, DB_MKT_FLAG).equals(mkflag) == false)) {
				new SetConfig(mContext).request(mflag, nflag, mkflag, null);
			}

			// readMsg
			final JSONArray readArray = PMSUtil.arrayFromPrefs(mContext, PREF_READ_LIST);
			if (readArray.length() > 0) {
				// call readMsg
				new ReadMsg(mContext).request(readArray, new APICallback() {
					@Override
					public void response (String code, JSONObject json) {
						if (CODE_SUCCESS.equals(code)) {
							// delete readMsg
							new Prefs(mContext).putString(PREF_READ_LIST, "");
						}

						if (CODE_PARSING_JSON_ERROR.equals(code)) {
							for (int i = 0; i < readArray.length(); i++) {
								try {
									if ((readArray.get(i) instanceof JSONObject) == false) {
										readArray.put(i, PMSUtil.getReadParam(readArray.getString(i)));
									}
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
							new ReadMsg(mContext).request(readArray, new APICallback() {
								@Override
								public void response (String code, JSONObject json) {
									if (CODE_SUCCESS.equals(code)) {
										new Prefs(mContext).putString(PREF_READ_LIST, "");
									}
								}
							});
						}
					}
				});
			} else {
				CLog.i("readArray is null");
			}

			// clickMsg
			JSONArray clickArray = PMSUtil.arrayFromPrefs(mContext, PREF_CLICK_LIST);
			if (clickArray.length() > 0) {
				// call clickMsg
				new ClickMsg(mContext).request(clickArray, new APICallback() {
					@Override
					public void response (String code, JSONObject json) {
						if (CODE_SUCCESS.equals(code)) {
							// delete clickMsg
							new Prefs(mContext).putString(PREF_CLICK_LIST, "");
						}
					}
				});
			} else {
				CLog.i("clickArray is null");
			}

			// mqttFlag Y/N
			if (FLAG_Y.equals(PMSUtil.getMQTTFlag(mContext))) {
				String flag = json.getString("privateFlag");
				DataKeyUtil.setDBKey(mContext, DB_PRIVATE_FLAG, flag);
				if (FLAG_Y.equals(flag)) {
					String protocol = json.getString("privateProtocol");
					String protocolTemp = "";
					try {
						URI uri = new URI(PMSUtil.getMQTTServerUrl(mContext));
						if (protocol.equals(PROTOCOL_TCP)) {
							protocolTemp = protocol.toLowerCase() + "cp";
						} else if (protocol.equals(PROTOCOL_SSL)) {
							protocolTemp = protocol.toLowerCase() + "sl";
						}

						if (uri.getScheme().equals(protocolTemp)) {
							DataKeyUtil.setDBKey(mContext, DB_PRIVATE_PROTOCOL, protocol);
						} else {
							DataKeyUtil.setDBKey(mContext, DB_PRIVATE_PROTOCOL, protocol);
							mContext.stopService(new Intent(mContext, MQTTService.class));
						}
					} catch (NullPointerException e) {
						DataKeyUtil.setDBKey(mContext, DB_PRIVATE_PROTOCOL, protocol);
					}

				} else {
					try
					{
						mContext.stopService(new Intent(mContext, MQTTService.class));
					}
					catch (Exception e) { }
				}
			}

			// LogFlag Y/N
			if ((FLAG_N.equals(DataKeyUtil.getDBKey(mContext, DB_API_LOG_FLAG)) && FLAG_N.equals(DataKeyUtil.getDBKey(mContext, DB_PRIVATE_LOG_FLAG))) == false) {
				setCollectLog();
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private void setCollectLog () {
		boolean isAfter = false;
		String beforeDate = DataKeyUtil.getDBKey(mContext, DB_YESTERDAY);
		String today = DateUtil.getNowDateMo();

		try {
			isAfter = DateUtil.isDateAfter(beforeDate, today);
		} catch (Exception e) {
			isAfter = false;
		}

		if (isAfter) {
			DataKeyUtil.setDBKey(mContext, DB_ONEDAY_LOG, "N");
		}

		if (FLAG_N.equals(DataKeyUtil.getDBKey(mContext, DB_ONEDAY_LOG)) == false) {
			if (beforeDate.equals("")) {
				beforeDate = DateUtil.getNowDateMo();
				DataKeyUtil.setDBKey(mContext, DB_YESTERDAY, beforeDate);
			}
			new CollectLog(mContext).request(beforeDate, null);
		}
	}
}
