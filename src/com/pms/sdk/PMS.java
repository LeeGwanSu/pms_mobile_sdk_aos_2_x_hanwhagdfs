package com.pms.sdk;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.MsgGrp;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DataKeyUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;

import java.io.Serializable;

/**
 * @since 2012.12.28
 * @author erzisk
 * @description pms (solution main class)
 * @version [2014.06.24 09:10] ReadMSg UserMsgId로 가능하게 수정함. <br>
 *          [2015.02.04 10:23] SDK 사용 중지 요청 가능하게 수정함. <br>
 *          [2015.02.05 19:33] newMsg Update시 DEL_YN값이 null값이 들어가는 현상 수정함. <br>
 *          [2015.02.11 09:55] Flag 값들이 안바뀌는 현상 수정함. <br>
 *          [2015.02.25 11:45] 앱 최초설치시에는 msgFlag & notiFlag 값 서버에서 받아온 값 저장함. <br>
 *          [2015.03.03 16:55] 앱실행시 체크하는 프로세스 롤리팝 버전에 맞게 수정함.<br>
 *          [2015.03.11 10:02] Private 서버 안정화.<br>
 *          [2015.03.17 18:11] 팝업창 노출 플래그 추가함.<br>
 *          [2015.03.19 16:29] DB쪽 버그 수정함.<br>
 *          [2015.03.30 21:34] 팝업 클릭시 ClickMsg & readMsg 직접호출로 수정함.<br>
 *          [2015.04.03 09:40] Notification Image Load가 실패하면 Text Notification으로 전환하는 코스 삽입함.<br>
 *          [2015.04.23 16:59] GCM 받아오는 부분 팝업창 뜨도록 되어 있는부분을 로그로 찍게 수정함.<br>
 *          [2015.06.12 10:21] GCM 미지원시 오류 처리함.<br>
 *          [2015.06.26 10:50] LogoutPms.m 호출시 MaxUserMsgId를 -1로 초기화 하는 루틴 수정함.<br>
 *          [2015.07.20 13:23] Notification Priority추가함.<br>
 *          [2015.07.20 16:43] BigImage시 메세지 내용도 보이게 수정함.<br>
 *          [2015.09.07 10:58] 팝업창 뛰우는 플래그 'N'으로 변경. MSG class pushImg 값 추가함.<br>
 *          [2015.10.13 15:35] Android 6.0 접근권한 체크하는 루틴 추가함.<br>
 *          [2015.10.21 13:53] Logout시 Max user Msg Id값 초기화 하는 루틴 추가.<br>
 *          [2015.12.30 14:05] MsgGrpCd가 문자열일때 오류나는 부분 수정함.<br>
 *          [2016.01.15 14:17] mktFlag 값 추가함.<br>
 *          [2016.02.25 16:54] MktFlag 정책 변경으로 인한 적용<br>
 *          [2016.03.08 13:26] Logout시에도 적용되도록 변경함.<br>
 *          [2016.03.21 14:21] selfsign을 없애고 getSignKey에서 SSL관련 개인보안키를 받도록 수정함.<br>
 *          [2016.04.29 13:23] X509TrustManager 코드 삭제하고 대응 코드 삽입함.<br>
 *          [2016.09.22 14:03] 정통망법 및 mktVer 플래그 추가함. <br>
 *          [2016.12.02 18:36] doze mode broadcast 패치1 적용 <br>
 *			[2017.10.30 11:50] * Queue Manager 추가 Single tone 으로 관리 (Image Loader 및 APIManager)
 *          					* DB 중복 Open으로 인해 강제종료 문제 수정
 *          					* PushPopup 발생 시 강제 종료 문제 수정
 *          					* UUID 생성 로직 변경, Device정보를 이용하여 UUID를 작성하는 경로 이전에 Device 정보 없이 발급하는 방법으로
 *          					  UUID 발급, 발급 실패 시 Device정보를 필요로 하는 UUID 발급 방법으로 발급 시도함 <br>
 *          [2017.11.06 09:49] * Token 받아오는 방법이 변경되어 수정, 다음 을 추가
 *          					GCMPushService(직접 Push를 받는 Service), GetGCMInstanceID (Token Refresh 동작 Listener service)
 *          					RegistrationIntentService (Token을 받아오는 IntentService)
 *          					* 암호화 키 변경됨, Decrypt할 때와 Encrypt 할 때의 암호화 키가 달라 DB에 추가	 <br>
 *          [2017.01.24 17:32] * Release FCM 적용 (FCMPushService, GsetFCMInstanceID 추가)
 *          					8.0 대응 MQTTJobService 추가(8.0 이상 동작, AlarmJobPingSender, ServiceProvider, MQTTJobService, MQTTScheduler)
 *          					BigTextMode 추가
 *			[2018.08.03 15:45] NotificationChannel 대응, 암시적 Intent 제한 대응, JobScheduler 6.0 이상 동작, WRITE_EXTERNAL_STORAGE 권한 제거, PopupUtil 설정 Preference에 저장하도록 대응
 *			[2018.08.03 17:04] FCM PushReceiver 오류수정
 *			[2018.08.14 17:40] MQTT 라이브러리 교체, WakeUp 푸시 예외처리, 8.0 진동 2번울리게함, Pmspopup 오류 수정
 *			[2018.09.05 11:21] MQTT 간소화 버전 적용, EnableUUID 대응, 이미지캐시대응, NotificationChannel 대응, 잠금화면 체크 추가
 *			[2018.09.12 13:37] MQTT 간소화 오류 대응, 벨소리 볼륨 대응 추가
 *			[2018.10.17 13:27] MQTT 오류대응 적용, 중복 푸시 안오게함
 *		    [2018.10.25 16:17] MQTT 수정
 *		    [2018.11.02 08:38] MQTT 수정
 *		    [2018.11.12 15:40] PushReceiver 버그 수정
 *		    [2019.02.26 10:18] FCMInstanceId Deprecated 대응, Volley 에러 찍게함
 *		    [2019.02.26 11:05] MQTTReceiver 적용
 *			[2019.09.05 14:39] 벨소리 볼륨 조절 로직 제거, 채널 강제 재생성 로직 제거, 암호화키 숨김 처리 적용, ReadMsg kk->HH 적용
 *			[2019.09.16 15:19] 복호화키 숨김 처리 적용
 *
 *
 *
 *
 */
public class PMS implements IPMSConsts, Serializable {

	private static final long serialVersionUID = 1L;

	private static PMS instancePms = null;

	private static PMSPopup instancePmsPopup = null;

	private Context mContext = null;

	private PMSDB mDB = null;

	private PMS(Context context) {
		this.mDB = PMSDB.getInstance(context);
		CLog.i("Version:" + PMS_VERSION + ",UpdateDate:201909161519");

		initOption(context);
	}

	public static PMS getInstance (Context context) {
		CLog.d("getInstance:projectId=" + PMSUtil.getGCMProjectId(context));
		CLog.setDebugMode(context);
		CLog.setDebugName(context);
		if (instancePms == null) {
			instancePms = new PMS(context);
		}
		instancePms.setmContext(context);
		return instancePms;
	}

	public static PMSPopup getPopUpInstance () {
		return instancePmsPopup;
	}

	public static boolean clear () {
		try {
			PMSUtil.setDeviceCertStatus(instancePms.mContext, DEVICECERT_PENDING);
			instancePms = null;
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private void initOption (Context context) {
		if (StringUtil.isEmpty(DataKeyUtil.getDBKey(context, DB_RING_FLAG))) {
			DataKeyUtil.setDBKey(context, DB_RING_FLAG, "Y");
		}
		if (StringUtil.isEmpty(DataKeyUtil.getDBKey(context, DB_VIBE_FLAG))) {
			DataKeyUtil.setDBKey(context, DB_VIBE_FLAG, "Y");
		}
		if (StringUtil.isEmpty(DataKeyUtil.getDBKey(context, DB_ALERT_FLAG))) {
			DataKeyUtil.setDBKey(context, DB_ALERT_FLAG, "N");
		}
		if (StringUtil.isEmpty(DataKeyUtil.getDBKey(context, DB_MAX_USER_MSG_ID))) {
			DataKeyUtil.setDBKey(context, DB_MAX_USER_MSG_ID, "-1");
		}
	}

	public void setCustId (String custId) {
		PMSUtil.setCustId(mContext, custId);
	}

	public String getCustId () {
		return PMSUtil.getCustId(mContext);
	}

	private void setmContext (Context context) {
		this.mContext = context;
	}

	public void setPopupSetting (Boolean state, String title) {
		instancePmsPopup = PMSPopup.getInstance(mContext, mContext.getPackageName(), state, title);
	}

	public void setIsPopupActivity (Boolean ispopup) {
		PMSUtil.setPopupActivity(mContext, ispopup);
	}

	public void setNotiOrPopup (Boolean isnotiorpopup) {
		PMSUtil.setNotiOrPopup(mContext, isnotiorpopup);
	}

	public void setRingMode (boolean isRingMode) {
		DataKeyUtil.setDBKey(mContext, DB_RING_FLAG, isRingMode ? "Y" : "N");
	}

	public void setVibeMode (boolean isVibeMode) {
		DataKeyUtil.setDBKey(mContext, DB_VIBE_FLAG, isVibeMode ? "Y" : "N");
	}

	public void setPopupNoti (boolean isShowPopup) {
		DataKeyUtil.setDBKey(mContext, DB_ALERT_FLAG, isShowPopup ? "Y" : "N");
	}

	public String getMsgFlag () {
		return DataKeyUtil.getDBKey(mContext, DB_MSG_FLAG);
	}

	public String getNotiFlag () {
		return DataKeyUtil.getDBKey(mContext, DB_NOTI_FLAG);
	}

	public String getMktFlag () {
		return DataKeyUtil.getDBKey(mContext, DB_MKT_FLAG);
	}

	public String getMaxUserMsgId () {
		return DataKeyUtil.getDBKey(mContext, DB_MAX_USER_MSG_ID);
	}

	/*
	 * ===================================================== [start] database =====================================================
	 */
	/**
	 * select MsgGrp list
	 * 
	 * @return
	 */
	public Cursor selectMsgGrpList () {
		return mDB.selectMsgGrpList();
	}

	/**
	 * select MsgGrp
	 * 
	 * @param msgCode
	 * @return
	 */
	public MsgGrp selectMsGrp (String msgCode) {
		return mDB.selectMsgGrp(msgCode);
	}

	/**
	 * select new msg Cnt
	 * 
	 * @return
	 */
	public int selectNewMsgCnt () {
		return mDB.selectNewMsgCnt();
	}

	/**
	 * select Msg List
	 * 
	 * @param page
	 * @param row
	 * @return
	 */
	public Cursor selectMsgList (int page, int row) {
		return mDB.selectMsgList(page, row);
	}

	/**
	 * select Msg List
	 * 
	 * @param msgCode
	 * @return
	 */
	public Cursor selectMsgList (String msgCode) {
		return mDB.selectMsgList(msgCode);
	}

	/**
	 * select Msg
	 * 
	 * @param msgId
	 * @return
	 */
	public Msg selectMsgWhereMsgId (String msgId) {
		return mDB.selectMsgWhereMsgId(msgId);
	}

	/**
	 * select Msg
	 * 
	 * @param userMsgID
	 * @return
	 */
	public Msg selectMsgWhereUserMsgId (String userMsgID) {
		return mDB.selectMsgWhereUserMsgId(userMsgID);
	}

	/**
	 * update MsgGrp
	 * 
	 * @param msgCode
	 * @param values
	 * @return
	 */
	public long updateMsgGrp (String msgCode, ContentValues values) {
		return mDB.updateMsgGrp(msgCode, values);
	}

	/**
	 * update read msg
	 * 
	 * @param msgGrpCd
	 * @param firstUserMsgId
	 * @param lastUserMsgId
	 * @return
	 */
	public long updateReadMsg (String msgGrpCd, String firstUserMsgId, String lastUserMsgId) {
		return mDB.updateReadMsg(msgGrpCd, firstUserMsgId, lastUserMsgId);
	}

	/**
	 * update read msg where userMsgId
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long updateReadMsgWhereUserMsgId (String userMsgId) {
		return mDB.updateReadMsgWhereUserMsgId(userMsgId);
	}

	/**
	 * update read msg where msgId
	 * 
	 * @param msgId
	 * @return
	 */
	public long updateReadMsgWhereMsgId (String msgId) {
		return mDB.updateReadMsgWhereMsgId(msgId);
	}

	/**
	 * delete Msg
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long deleteUserMsgId (String userMsgId) {
		return mDB.deleteUserMsgId(userMsgId);
	}

	/**
	 * delete Msg
	 * 
	 * @param MsgId
	 * @return
	 */
	public long deleteMsgId (String MsgId) {
		return mDB.deleteMsgId(MsgId);
	}

	/**
	 * delete MsgGrp
	 * 
	 * @param msgCode
	 * @return
	 */
	public long deleteMsgGrp (String msgCode) {
		return mDB.deleteMsgGrp(msgCode);
	}

	/**
	 * delete expire Msg
	 * 
	 * @return
	 */
	public long deleteExpireMsg () {
		return mDB.deleteExpireMsg();
	}

	/**
	 * delete empty MsgGrp
	 * 
	 * @return
	 */
	public void deleteEmptyMsgGrp () {
		mDB.deleteEmptyMsgGrp();
	}

	/**
	 * delete all
	 */
	public void deleteAll () {
		mDB.deleteAll();
	}

	/*
	 * ===================================================== [end] database =====================================================
	 */
}
